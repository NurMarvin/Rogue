**Rogue**

The Rogue Anti Cheat Base is an advanced anti cheat base for [Spigot](htts://spigotmc.org) for the game [Minecraft](https://minecraft.net).

It should help anti cheat creators to have a base to start off with instead of starting from scratch and the need to do all the tedious work of implementing packets, logging etc.

**Installation**

1. Clone the repository using `git clone https://gitlab.com/NurMarvin/Rogue.git`
2. Use Eclipse or IntelliJ to import the Maven Project from the pom.xml
3. Happy coding :)

If you need any help regarding installation or usage, you may message me on Discord: `NurMarvin#1337`. 

**Usage conditions**

1. When using Rogue as a base for your Anti Cheat, you must keep the [License](https://gitlab.com/NurMarvin/Rogue/LICENSE.md).
2. **YOU MUST** name me in the credits of your Anti Cheat - websites **AND** plugin.

**Contribution**

If you want to contribute to Rogue, you may do this under the condition of following the [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html).
This is the only condition for now, but this may change at any point.