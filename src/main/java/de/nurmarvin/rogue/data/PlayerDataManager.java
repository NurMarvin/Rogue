package de.nurmarvin.rogue.data;

import com.google.common.collect.Maps;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class PlayerDataManager {
    private HashMap<UUID, PlayerData> playerData;

    public PlayerDataManager() {
        this.playerData = Maps.newHashMap();
    }

    public PlayerData getPlayerData(Player player) {
        if(!this.playerData.containsKey(player.getUniqueId())) this.playerData.put(player.getUniqueId(),
                                                                                   new PlayerData(player));
        return this.playerData.get(player.getUniqueId());
    }
}
