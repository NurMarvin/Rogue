package de.nurmarvin.rogue.data;

public class ViolationData {
    private String details;

    public ViolationData(String details) {
        this.details = details;
    }

    public String getDetails() {
        return details;
    }
}
