package de.nurmarvin.rogue.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.nurmarvin.rogue.check.Check;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayerData {
    private Player player;
    private HashMap<String, ArrayList<ViolationData>> violations;

    public PlayerData(Player player) {
        this.player = player;
        this.violations = Maps.newHashMap();
    }

    public void addViolation(String check, String details) {
        ArrayList<ViolationData> violationData = this.violations.get(check);

        if(violationData == null) violationData = Lists.newArrayList();
        
        violationData.add(new ViolationData(details));
        violations.put(check, violationData);

        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if(onlinePlayer.isOp() || onlinePlayer.hasPermission("rogue.notify"))
            onlinePlayer.sendMessage(player.getName() + " failed " + check + ": " + details);
        }
    }
}
