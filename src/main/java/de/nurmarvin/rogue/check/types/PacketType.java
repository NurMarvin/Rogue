package de.nurmarvin.rogue.check.types;

import de.nurmarvin.rogue.check.CheckType;
import de.nurmarvin.rogue.event.events.PacketReceivedEvent;
import de.nurmarvin.rogue.event.events.PacketSentEvent;

public class PacketType extends CheckType {
    public PacketType(String name, boolean enabled) {
        super(name, enabled);
    }

    public void onPacketReceived(PacketReceivedEvent event) {}
    public void onPacketSent(PacketSentEvent event) {}
}
