package de.nurmarvin.rogue.check.types;

import de.nurmarvin.rogue.check.CheckType;
import de.nurmarvin.rogue.event.events.RogueAsyncPlayerMoveEvent;

public class MovementType extends CheckType {

    public MovementType(String name, boolean enabled) {
        super(name, enabled);
    }

    public void onMove(RogueAsyncPlayerMoveEvent event) {}
}
