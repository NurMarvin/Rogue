package de.nurmarvin.rogue.check;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.nurmarvin.rogue.Rogue;
import de.nurmarvin.rogue.check.types.MovementType;
import de.nurmarvin.rogue.check.types.PacketType;
import de.nurmarvin.rogue.event.events.PacketReceivedEvent;
import de.nurmarvin.rogue.event.events.PacketSentEvent;
import de.nurmarvin.rogue.event.events.RogueAsyncPlayerMoveEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;

public class CheckManager implements Listener {
    private HashMap<CheckType.Type, ArrayList<Check>> checks;
    private HashMap<CheckType.Type, ArrayList<CheckType>> checkTypes;

    public CheckManager() {
        this.checks = Maps.newHashMap();
        this.checkTypes = Maps.newHashMap();
    }

    public void addCheck(Check check) {
        ArrayList<Check> checks = this.checks.get(check.getType());

        if(checks == null) checks = Lists.newArrayList();

        checks.add(check);

        this.checks.put(check.getType(), checks);
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        RogueAsyncPlayerMoveEvent rogueAsyncPlayerMoveEvent = new RogueAsyncPlayerMoveEvent(event.getPlayer(),
                                                                                            event.getFrom(), event.getTo());
        new BukkitRunnable() {
            @Override
            public void run() {
                Bukkit.getPluginManager().callEvent(rogueAsyncPlayerMoveEvent);

                ArrayList<CheckType> checkTypes = getCheckTypesOfType(CheckType.Type.MOVEMENT);

                if(checkTypes != null)
                    checkTypes.forEach(c -> ((MovementType) c).onMove(rogueAsyncPlayerMoveEvent));

                if(rogueAsyncPlayerMoveEvent.isCancelled()) {
                    //Create a new location with the coordinates but with old Yaw and Pitch to prevent nasty camera
                    // snaps
                    Location location = new Location(event.getFrom().getWorld(), event.getFrom().getX(),
                                                     event.getFrom().getY(), event.getFrom().getZ());
                    location.setYaw(event.getTo().getYaw());
                    location.setYaw(event.getTo().getPitch());
                    Bukkit.getScheduler().runTask(Rogue.getInstance(), () -> event.getPlayer().teleport(location));
                }

            }
        }.runTaskAsynchronously(Rogue.getInstance());
    }

    @EventHandler
    public void onPacketReceived(PacketReceivedEvent event) {
        ArrayList<CheckType> checkTypes = getCheckTypesOfType(CheckType.Type.PACKET);

        if(checkTypes != null)
        checkTypes.forEach(c -> ((PacketType) c).onPacketReceived(event));
    }

    @EventHandler
    public void onPacketSent(PacketSentEvent event) {
        ArrayList<CheckType> checkTypes = getCheckTypesOfType(CheckType.Type.PACKET);

        if(checkTypes != null)
        checkTypes.forEach(c -> ((PacketType) c).onPacketSent(event));
    }

    private ArrayList<CheckType> getCheckTypesOfType(CheckType.Type type) {
        if(this.checkTypes.containsKey(type)) return this.checkTypes.get(type);

        ArrayList<CheckType> checkTypes = Lists.newArrayList();

        if(!this.checks.containsKey(type)) {
            return null;
        }

        for(Check check : this.checks.get(type)) {
            checkTypes.addAll(check.getChecksTypes());
        }

        return this.checkTypes.put(type, checkTypes);
    }

    public Check getCheckByName(String name) {
        for (ArrayList<Check> checks : this.checks.values()) {
            for (Check check : checks) {
                if(check.getName().equalsIgnoreCase(name)) {
                    return check;
                }
            }
        }
        return null;
    }

    public HashMap<CheckType.Type, ArrayList<Check>> getChecks() {
        return checks;
    }

    public HashMap<CheckType.Type, ArrayList<CheckType>> getCheckTypes() {
        return checkTypes;
    }
}
