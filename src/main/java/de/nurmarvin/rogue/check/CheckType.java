package de.nurmarvin.rogue.check;

public class CheckType {
    private String name;
    private boolean enabled;

    public CheckType(String name, boolean enabled) {
        this.name = name;
        this.enabled = enabled;
    }

    public enum Type {
        PACKET,
        MOVEMENT
    }

    public String getName() {
        return name;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
