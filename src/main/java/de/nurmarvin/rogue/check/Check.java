package de.nurmarvin.rogue.check;

import com.google.common.collect.Lists;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class Check {
    @Expose
    private String name;
    @Expose
    private long violationsToCancel, violationsToBan;
    private ArrayList<CheckType> checksTypes;
    private CheckType.Type type;
    @Expose
    private boolean enabled;

    public Check(String name, CheckType.Type type, boolean enabled, long violationsToCancel, long violationsToBan) {
        this.name = name;
        this.type = type;
        this.enabled = enabled;
        this.violationsToCancel = violationsToCancel;
        this.violationsToBan = violationsToBan;
        this.checksTypes = Lists.newArrayList();
    }

    public void addCheckType(CheckType checkType) {
        this.checksTypes.add(checkType);
    }

    public ArrayList<CheckType> getChecksTypes() {
        return checksTypes;
    }

    public CheckType.Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public long getViolationsToBan() {
        return violationsToBan;
    }

    public long getViolationsToCancel() {
        return violationsToCancel;
    }

    public boolean isEnabled() {
            return enabled;
    }
}
