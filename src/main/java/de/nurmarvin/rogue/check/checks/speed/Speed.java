package de.nurmarvin.rogue.check.checks.speed;

import com.google.gson.annotations.Expose;
import de.nurmarvin.rogue.check.Check;
import de.nurmarvin.rogue.check.CheckType;
import de.nurmarvin.rogue.check.checks.speed.type.TypeA;

public class Speed extends Check {

    @Expose
    private float maxSpeed;

    public Speed() {
        super("Speed", CheckType.Type.MOVEMENT, true, 0, 0);
        this.addCheckType(new TypeA());
    }
}
