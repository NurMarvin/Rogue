package de.nurmarvin.rogue.check.checks.speed.type;

import de.nurmarvin.rogue.check.types.MovementType;
import de.nurmarvin.rogue.event.events.RogueAsyncPlayerMoveEvent;

public class TypeA extends MovementType {
    public TypeA() {
        super("TypeA", true);
    }

    @Override
    public void onMove(RogueAsyncPlayerMoveEvent event) {
        if(event.getFrom().distance(event.getTo()) > 1) {
            event.setCancelled(true);
            event.flag("Speed", "Obscure move");
        }
    }
}
