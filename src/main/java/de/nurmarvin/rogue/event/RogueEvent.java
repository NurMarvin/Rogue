package de.nurmarvin.rogue.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public abstract class RogueEvent extends Event {

    public RogueEvent() {

    }

    public RogueEvent(boolean isAsync) {
        super(isAsync);
    }
}
