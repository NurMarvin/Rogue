package de.nurmarvin.rogue.event.events;

import com.google.common.base.Preconditions;
import de.nurmarvin.rogue.event.RoguePlayerEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class RogueAsyncPlayerMoveEvent extends RoguePlayerEvent implements Cancellable {
    private Location from, to;
    private boolean cancel;
    private static HandlerList handlerList = new HandlerList();

    public RogueAsyncPlayerMoveEvent(Player player, Location from, Location to) {
        super(true, player);
        this.from = from;
        this.to = to;
        this.cancel = false;
    }

    public void setFrom(Location from) {
        validateLocation(from);
        this.from = from;
    }

    public Location getFrom() {
        return from;
    }

    public void setTo(Location to) {
        validateLocation(to);
        this.to = to;
    }

    public Location getTo() {
        return to;
    }

    @Override
    public boolean isCancelled() {
        return this.cancel;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }

    private void validateLocation(Location loc) {
        Preconditions.checkArgument(loc != null, "Cannot use null location!");
        Preconditions.checkArgument(loc.getWorld() != null, "Cannot use null location with null world!");
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
