package de.nurmarvin.rogue.event.events;

import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import de.nurmarvin.rogue.event.RogueEvent;
import de.nurmarvin.rogue.event.RoguePlayerEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public final class PacketReceivedEvent extends RoguePlayerEvent {
    private PacketContainer packetContainer;
    private static HandlerList handlerList = new HandlerList();

    public PacketReceivedEvent(Player  player, PacketContainer packetContainer) {
        super(player);
        this.packetContainer = packetContainer;
    }

    public PacketContainer getPacketContainer() {
        return packetContainer;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
