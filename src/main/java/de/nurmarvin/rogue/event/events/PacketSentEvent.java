package de.nurmarvin.rogue.event.events;

import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import de.nurmarvin.rogue.event.RoguePlayerEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public final class PacketSentEvent extends RoguePlayerEvent {
    private PacketContainer packetContainer;
    private static HandlerList handlerList = new HandlerList();

    public PacketSentEvent(Player  player, PacketContainer packetContainer) {
        super(player);
        this.packetContainer =  packetContainer;
    }

    public PacketContainer getPacketContainer() {
        return packetContainer;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
