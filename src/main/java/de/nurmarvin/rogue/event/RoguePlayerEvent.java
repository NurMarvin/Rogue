package de.nurmarvin.rogue.event;

import de.nurmarvin.rogue.Rogue;
import de.nurmarvin.rogue.data.PlayerData;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class RoguePlayerEvent extends RogueEvent {

    private Player player;
    private PlayerData playerData;
    private static HandlerList handlerList = new HandlerList();

    public RoguePlayerEvent(Player player) {
        this.player = player;
        this.playerData = Rogue.getInstance().getPlayerDataManager().getPlayerData(player);
    }

    public RoguePlayerEvent(boolean isAsync, Player player) {
        super(isAsync);
        this.player = player;
        this.playerData = Rogue.getInstance().getPlayerDataManager().getPlayerData(player);
    }

    public Player getPlayer() {
        return player;
    }

    public PlayerData getPlayerData() {
        return this.playerData;
    }

    public void flag(String check, String details) {
        this.playerData.addViolation(check, details);
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
