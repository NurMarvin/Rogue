package de.nurmarvin.rogue;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import de.nurmarvin.rogue.check.CheckManager;
import de.nurmarvin.rogue.check.checks.speed.Speed;
import de.nurmarvin.rogue.data.PlayerDataManager;
import de.nurmarvin.rogue.event.events.PacketReceivedEvent;
import de.nurmarvin.rogue.event.events.PacketSentEvent;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class Rogue extends JavaPlugin {

    private static Rogue instance;
    private CheckManager checkManager;
    private PlayerDataManager playerDataManager;

    @Override
    public void onEnable() {
        instance = this;

        this.checkManager = new CheckManager();
        this.playerDataManager = new PlayerDataManager();
        this.getServer().getPluginManager().registerEvents(this.checkManager, this);

        this.registerChecks();

        if(this.getServer().getPluginManager().getPlugin("ProtocolLib") == null) {
            Bukkit.getConsoleSender().sendMessage("§cRogue requires you to have ProtocolLib installed to work. " +
                                                  "Disabling...");
            this.getServer().getPluginManager().disablePlugin(this);
        } else {
            Bukkit.getConsoleSender().sendMessage("§aFound ProtocolLib, hooking in...");
            this.handlePackets();
        }

    }

    @Override
    public void onDisable() {

    }

    public void registerChecks() {
        this.checkManager.addCheck(new Speed());
    }

    public void handlePackets() {
        ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();

        protocolManager.addPacketListener(
                new PacketAdapter(this, ListenerPriority.NORMAL,
                                  PacketType.Play.Server.getInstance().values()) {
                    @Override
                    public void onPacketSending(PacketEvent event) {
                        if(event.getPlayer() != null)
                        Bukkit.getPluginManager().callEvent(new PacketSentEvent(event.getPlayer(), event.getPacket()));
                    }
                });

        protocolManager.addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL,
                                                            PacketType.Play.Client.getInstance().values()) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                if(event.getPlayer() != null)
                Bukkit.getPluginManager().callEvent(new PacketReceivedEvent(event.getPlayer(), event.getPacket()));
            }
        });
    }

    public static Rogue getInstance() {
        return instance;
    }

    public PlayerDataManager getPlayerDataManager() {
        return playerDataManager;
    }

    public CheckManager getCheckManager() {
        return checkManager;
    }
}
